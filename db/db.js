var mongoose = require('mongoose');

//this database is a mongo server on a cheap linux box in my family room
//the docs say that auto reconnect is on by default, so I won't explicitly set it.
mongoose.connect('mongodb://read:read@abradford.com:27011/project');


//log on connection
mongoose.connection.on('connected', function () {
    console.log('Mongoose default connection open');
});

//if there was a error
mongoose.connection.on('error',function (err) {
    console.log('Mongoose default connection error: ' + err);
});

//when a disconnection happens
mongoose.connection.on('disconnected', function () {
    console.log('Mongoose default connection disconnected');
});

//set up the schema/model
require("./thumbnail.js");
