//sets up the schema for the thumbnail search

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Thumbnail =  new Schema({

        offset: Number,
        programId: String,
        url: String,
        smallUrl: String,
        _id: String
    });

module.exports = mongoose.model("records", Thumbnail);
