
//set up express
var express = require('express');
var app = express();

app.configure(function() {
    app.use(express.logger('dev'));
    app.use(express.bodyParser());
    app.use(express.methodOverride());
});

//set up routes
var routes = require('./routes');

//The routes we are worried about
app.get('/thumbnailsBetween/start/:startTime/end/:endTime', routes.thumbnailsBetween);

//everything else
app.get('/*', function(req,res){
    res.status(404);
    res.send('Usage: host/thumbnailsBetween/start/(startOffset)/end/(endOffset)');
});

//set up the database connection
var db = require('./db/db.js');

// start up the server on port 3000
var port = 3000;
app.listen(port);
console.log("listening on port " + port);
