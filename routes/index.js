//routes.js

//this is the route we are worried about in this homework
exports.thumbnailsBetween = function(req, res){

    //check if we have valid integers as inputs
	if(!checkIfInt(req.params.startTime, req.params.endTime))
	{
		res.status(404);
    	res.send('Invalid Request');
	}

    //we have integers, so we can parse them
	var startInt = parseInt(req.params.startTime);
	var endInt = parseInt(req.params.endTime);

    //check if numbers are greater than 0
    if(endInt < 0 || startInt < 0)
    {
        res.status(404);
        res.send('Times must be greater than 0');
    }

    //test if start comes before end
	if(endInt < startInt)
	{
		res.status(404);
    	res.send('Start time less than end time');
	}

    //now we can finally query the database

    //get the mongoose instance
    var mongoose = require('mongoose');
    //set the schema
    var schema = require('../db/thumbnail.js').Thumbnail;
    //get the mode.
    var Thumbnail = mongoose.model('records', schema);

    //query the database with the model
    Thumbnail.find({})
        .where('offset').gte(startInt).lte(endInt)
        .sort([['offset' , 1]])
        .exec(function(error, results){
        if(error)
        {
            res.status(500);
            res.send('Error accessing database');
            console.log(err);
        }
        else
        {
            //we have the results! send them back to the requester
            console.log(results.length);
            res.json(results);

        }
    });



};


//a simple function to check if the inputs are integers.
function checkIfInt(start, end)
{
	var startInt = parseInt(start);
	var endInt = parseInt(end);

	if(isNaN(startInt) || isNaN(endInt))
		return false;

    return !(start != startInt || end != endInt);


}